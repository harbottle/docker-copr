FROM centos:latest
MAINTAINER harbottle
RUN yum -y install epel-release
RUN yum -y install copr-cli
RUN yum -y install rpm-build
RUN yum -y install rpmdevtools
RUN yum -y install wget
RUN yum -y install dpkg
